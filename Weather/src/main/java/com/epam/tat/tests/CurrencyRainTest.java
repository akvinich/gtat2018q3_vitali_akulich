package com.epam.tat.tests;

import com.epam.tat.api.CurrencyDay;
import com.epam.tat.api.WeatherDay;
import com.epam.tat.loger.Log;
import com.epam.tat.runner.Parameters;
import io.restassured.RestAssured;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.*;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class CurrencyRainTest {

    public static final  String XPATH_PATTERN = "//weather";
    public static final String PREF_BANK_URL = "http://www.nbrb.by/API/ExRates/Rates/145?onDate=";
    public static final double DESIRED_GROWTH_MARK = 0.5;
    public static final String[] STATE_HOLIDAY_DATES = {
        "2017-01-01",
        "2017-01-07",
        "2017-03-08",
        "2017-05-01",
        "2017-05-09",
        "2017-06-03",
        "2017-11-07",
        "2017-12-25"};

    private String startDate = Parameters.instance().getStartDate();
    private String endDate = Parameters.instance().getEndDate();
    private XPathFactory xPathFactory;
    private XPath xPath;
    private Document doc;
    private List<WeatherDay> weatherDayList = new ArrayList<>();

    @BeforeTest
    public void getDays() throws ParserConfigurationException, IOException, SAXException, XPathExpressionException {
        boolean isNotLastDate = true;
        while (isNotLastDate) {
            Log.info("Request to " + "http://api.worldweatheronline.com");
            String generatedUrl = "http://api.worldweatheronline.com/premium/v1/past-weather.ashx"
                    + "?key=f6611b862be94daba96214702192901&q=Homyel&format=xml&date=" + startDate
                    + "&enddate=" + endDate;
            InputStream response = RestAssured.given().when().get(generatedUrl).asInputStream();
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            factory.setNamespaceAware(true);
            DocumentBuilder builder = factory.newDocumentBuilder();
            doc = builder.parse(response);
            xPathFactory = XPathFactory.newInstance();
            xPath = xPathFactory.newXPath();
            XPathExpression expr = xPath.compile(XPATH_PATTERN);
            Object result = expr.evaluate(doc, XPathConstants.NODESET);
            NodeList nodes = (NodeList) result;
            for (int i = 0; i < nodes.getLength(); i++) {
                WeatherDay weatherDay = new WeatherDay(nodes.item(i));
                if (endDate.equals(weatherDay.getDate())) {
                    weatherDayList.add(weatherDay);
                    Log.info("Added DAY: " + weatherDay.toString());
                    isNotLastDate = false;
                    break;
                } else {
                    weatherDayList.add(weatherDay);
                    Log.info("Added DAY: " + weatherDay.toString());
                    if (i == nodes.getLength() - 1) {
                        WeatherDay nextWeatherDay = this.addSomeDaysOfYear(weatherDay, 1);
                        startDate = nextWeatherDay.getDate();
                    }
                }
            }
            Log.info("Response from " + "http://api.worldweatheronline.com" + " processed");
        }
    }

    @Test
    public void checkRainTheory() {
        String generatedStartDayDate;
        String generatedNextDate;
        int rainDaysCount = 0;
        int riseRateDaysCount = 0;
        for (WeatherDay weatherDay : weatherDayList) {
            if (weatherDay.isRain() && weatherDay.getDay().get(Calendar.DAY_OF_WEEK) == Calendar.THURSDAY) {
                rainDaysCount++;
                Log.info("START analysis DAY: " + weatherDay.toString());
                generatedStartDayDate = weatherDay.getDate();
                if (this.isStateHolidayDate(generatedStartDayDate)) {
                    generatedStartDayDate = this.addSomeDaysOfYear(weatherDay, -1).getDate();
                }
                WeatherDay nextWeatherDay = this.addSomeDaysOfYear(weatherDay, 1);
                generatedNextDate = nextWeatherDay.getDate();
                if (this.isStateHolidayDate(generatedNextDate)) {
                    generatedNextDate = this.addSomeDaysOfYear(nextWeatherDay, 1).getDate();
                }
                CurrencyDay currencyStartDay = RestAssured.get(PREF_BANK_URL
                        + generatedStartDayDate).as(CurrencyDay.class);
                CurrencyDay currencyNextDay = RestAssured.get(PREF_BANK_URL
                        + generatedNextDate).as(CurrencyDay.class);
                double startDayRate = currencyStartDay.getCurOfficialRate();
                double nextDayRate = currencyNextDay.getCurOfficialRate();
                double differenceStartNext = nextDayRate - startDayRate;
                Log.info("startDayRate: " + startDayRate);
                Log.info("nextDayRate: " + nextDayRate);
                if (differenceStartNext > 0) {
                    riseRateDaysCount++;
                }
                Log.info("FINISH analysis DAY: " + weatherDay.toString());
            }
        }
        double percentOfRiseRateDays =  new Double(riseRateDaysCount) / new Double(rainDaysCount);
        Log.info("percentOfRiseRateDays = " + percentOfRiseRateDays);
        boolean isRiseRateAfterThursday = percentOfRiseRateDays > DESIRED_GROWTH_MARK;
        Assert.assertEquals(isRiseRateAfterThursday, true,
                "USD/BYN rate had been rising after every Thursday’s rain in Gomel in 2017: ");
    }

    private WeatherDay addSomeDaysOfYear(WeatherDay weatherDay, int countDays) {
        WeatherDay cloneWeatherDay = null;
        try {
            cloneWeatherDay = weatherDay.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
        cloneWeatherDay.getDay().add(Calendar.DAY_OF_YEAR, countDays);
        return cloneWeatherDay;
    }

    private boolean isStateHolidayDate(String date) {
        for (int i = 0; i < STATE_HOLIDAY_DATES.length; i++) {
            if (date.equals(STATE_HOLIDAY_DATES[i])) {
                return true;
            }
        }
        return false;
    }

}

package com.epam.tat.runner;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.ParameterException;
import com.epam.tat.loger.Log;
import org.testng.TestNG;

public class TestRunner {
    public static TestNG createTestNG() {
        TestNG testNG = new TestNG();
        testNG.setTestSuites(Parameters.instance().getTestConfig());
        return testNG;
    }

    public static void main(String[] args) {
        Log.info("Parse cli");
        TestRunner.parseCli(args);
        Log.info("Start app");
        TestRunner.createTestNG().run();
        Log.info("Finish app");
    }

    private static void parseCli(String[] args) {
        JCommander jCommander = new JCommander(Parameters.instance());
        try {
            jCommander.parse(args);
        } catch (ParameterException e) {
            Log.error(e.getMessage());
            jCommander.usage();
            System.exit(1);
        } catch (IllegalArgumentException e) {
            System.out.println("Incorrect data inputs: " + e.getMessage());
            jCommander.usage();
            System.exit(1);
        }
        if (Parameters.instance().isHelp()) {
            jCommander.usage();
            System.exit(0);
        }
    }
}

package com.epam.tat.runner;

import com.beust.jcommander.Parameter;
import com.beust.jcommander.converters.IParameterSplitter;

import java.util.Arrays;
import java.util.List;

public final class Parameters {
    private static Parameters instance;

    @Parameter(names = {"--help"}, help = false, description = "Help")
    private boolean help;

    @Parameter(names = {"--suites", "-s"}, description = "Path to test suites", splitter = SuitesSplitter.class,
            required = true)
    private List<String> testConfig = Arrays.asList(new String[]{"./src/main/resources/testng.xml"});

    @Parameter(names = {"--start", "-sd"}, description = "Start date")
    private String startDate = "2017-01-01";

    @Parameter(names = {"--end", "-ed"}, description = "End date")
    private String endDate = "2018-01-01";

    private Parameters() {
    }

    public boolean isHelp() {
        return help;
    }

    public List<String> getTestConfig() {
        return testConfig;
    }

    public String getStartDate() {
        return startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public static synchronized Parameters instance() {
        if (instance == null) {
            instance = new Parameters();
        }
        return instance;
    }

    public static class SuitesSplitter implements IParameterSplitter {
        public List<String> split(String value) {
            return Arrays.asList(value.split(";"));
        }
    }
}

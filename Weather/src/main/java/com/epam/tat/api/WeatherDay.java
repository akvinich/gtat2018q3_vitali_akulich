package com.epam.tat.api;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.util.*;

public class WeatherDay implements Cloneable {

    private boolean isRain;
    private Calendar day;

    public WeatherDay(Node node) {
        NodeList nodes = node.getChildNodes();
        int tagNodeSize = nodes.getLength();
        boolean isMinTempAboveZero = false;
        boolean isPrecipitation = false;
        List<Node> nodeList = new ArrayList<>();
        for (int i = 0; i < tagNodeSize; i++) {
            Node introWeatherNode = nodes.item(i);
            switch (introWeatherNode.getNodeName()) {
                case "date":
                    day = createDay(introWeatherNode);
                    break;
                case "mintempC":
                    isMinTempAboveZero = this.isMinTempAboveZero(introWeatherNode);
                    if (!isMinTempAboveZero) {
                        i = tagNodeSize;
                    }
                    break;
                case "hourly":
                    nodeList.add(introWeatherNode);
                    break;
                default:
                    break;
            }
        }
        isPrecipitation = this.isPrecipitation(nodeList);
        isRain = isMinTempAboveZero && isPrecipitation;
    }

    public boolean isRain() {
        return isRain;
    }

    public Calendar getDay() {
        return day;
    }

    public String getDate() {
        return day.get(Calendar.YEAR) + "-" + String.format("%02d", day.get(Calendar.MONTH) + 1)
                + "-" + String.format("%02d", day.get(Calendar.DAY_OF_MONTH));
    }

    private Calendar createDay(Node node) {
        String[] date = node.getTextContent().split("-");
        return new GregorianCalendar(
                Integer.parseInt(date[0]), Integer.parseInt(date[1]) - 1, Integer.parseInt(date[2]));
    }

    private  boolean isMinTempAboveZero(Node node) {
        if (Integer.parseInt(node.getTextContent()) > 0) {
            return true;
        }
        return false;
    }

    private boolean isPrecipitation(List<Node> nodeList) {
        int nodeListSize = nodeList.size();
        for (int i = 0; i < nodeListSize; i++) {
            NodeList hourlyNodes = nodeList.get(i).getChildNodes();
            int hourlyNodesSize = hourlyNodes.getLength();
            for (int j = 0; j < hourlyNodesSize; j++) {
                Node introHourlyNode = hourlyNodes.item(j);
                if (introHourlyNode.getNodeName().equals("precipMM")) {
                    if (Double.parseDouble(introHourlyNode.getTextContent()) > 0) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    @Override
    public String toString() {
        return "WeatherDay{" + "isRain=" + isRain + ", day=" + getDate() + " }";
    }

    @Override
    public WeatherDay clone() throws CloneNotSupportedException {
        final WeatherDay clone;
        clone = (WeatherDay) super.clone();
        clone.isRain = this.isRain;
        clone.day = (Calendar) this.day.clone();
        return clone;
    }
}

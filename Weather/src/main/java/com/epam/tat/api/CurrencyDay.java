package com.epam.tat.api;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties({"Cur_ID", "Date", "Cur_Abbreviation", "Cur_Scale", "Cur_Name"})
public class CurrencyDay {

    @JsonProperty("Cur_OfficialRate")
    private double curOfficialRate;

    public double getCurOfficialRate() {
        return curOfficialRate;
    }
}

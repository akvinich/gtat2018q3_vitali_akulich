package com.epam.tat.loger;

import org.apache.log4j.*;

public class Log {

    private static Logger logger = Logger.getLogger("LOGGER: ");

    public static void info(String message) {
        logger.info(message);
    }

    public static void error(String message) {
        logger.error(message);
    }

}

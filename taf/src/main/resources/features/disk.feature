    Feature:  Yandex disk features
        As a Yandex mail user
        I want to check main disk features

        Background:
            Given init valid account
              | email                   |  password       |
              | akvinich-test@yandex.ru |  akvinich600    |
            And user is on main yandex disk page

        @all
        Scenario Outline: User visits widgets
            When user clicks point of menu with mark: <mark>
            Then user visits widget with server folder: <widget>

        Examples:
          | mark     |  widget    |
          | mail     |  mail      |
          | recent   |  recent    |
          | photo    |  photo     |
          | trash    |  trash     |
          | ed       |  published |
          | disk     |  disk      |


        @all
        Scenario: User creates new folder on the disk
            Given user creates file entity
            When user creates new folder
            Then user visits new folder

        @all
        Scenario: User creates new word file in the folder
            Given user takes file entity
            When user creates new word file
            Then user visits new word file
            And close word file

        @all
        Scenario: User deletes word file
            Given user takes file entity
            When user deletes file permanently
            Then user does not find word file in the folder
            And user does not find word file in the trash


        @all
        Scenario: User stops browser
            Then user stops browser
    Feature:  Yandex login
        As a Yandex mail user
        I want to reach mail box after successful login

      Background:
          Given user is on login form

      @all
      Scenario: Successful login with valid credentials
          Given user has valid credentials
          When user submit login form
          Then user is reaching mailbox
          And user stops browser

      @all
      @negative
      Scenario: Unsuccessful login with invalid credentials
          Given user has invalid credentials
          When user submit login form
          Then user is not reaching mailbox
          And user stops browser
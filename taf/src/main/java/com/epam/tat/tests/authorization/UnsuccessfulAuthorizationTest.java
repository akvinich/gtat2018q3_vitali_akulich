package com.epam.tat.tests.authorization;

import com.epam.tat.framework.bo.Account;
import com.epam.tat.tests.DataProviders;
import com.epam.tat.yandex.product.disk.screen.app.YandexAccountPage;
import com.epam.tat.tests.BaseTest;
import com.epam.tat.yandex.product.disk.service.AccountService;
import org.testng.Assert;
import org.testng.annotations.Test;

public class UnsuccessfulAuthorizationTest extends BaseTest {

    @Test(dataProvider = "Invalid data for login", dataProviderClass = DataProviders.class)
    public void unsuccessfulLogInUserTest(Account account) {
        new AccountService().logIn(account);
        Assert.assertEquals(new YandexAccountPage().isOnThisPage(), false);
    }

}

package com.epam.tat.tests.app;

import com.epam.tat.framework.bo.Account;
import com.epam.tat.framework.bo.File;
import com.epam.tat.framework.bo.WidgetMarks;
import com.epam.tat.framework.exception.CustomException;
import com.epam.tat.tests.BaseTest;
import com.epam.tat.tests.DataProviders;
import com.epam.tat.yandex.product.disk.screen.app.MainYandexDiskPage;
import com.epam.tat.yandex.product.disk.service.AccountService;
import com.epam.tat.yandex.product.disk.service.DiskService;
import org.testng.Assert;
import org.testng.annotations.*;

public class DiskTest extends BaseTest {

    private MainYandexDiskPage mainYandexDiskPage;
    private Account account = Account.newEntity()
            .withEmail("akvinich-test@yandex.ru")
            .withPassword("akvinich600")
            .build();
    private DiskService diskService = new DiskService();

    @BeforeClass
    public void login() {
        new AccountService().logIn(account);
    }

    @Test(dataProvider = "Internal widgets marks", dataProviderClass = DataProviders.class)
    public void accessToWidgetsTest(WidgetMarks marks) {
        diskService.visitWidget(marks);
        mainYandexDiskPage = new MainYandexDiskPage();
        if (mainYandexDiskPage.isOnThisPage()) {
            if (marks.getEdgeAddress().equals("published")) {
                Assert.assertEquals(isValidMark(), true);
            } else {
                Assert.assertEquals(mainYandexDiskPage
                        .getCurrentWorkFolder(), marks.getEdgeAddress());
            }
        } else {
            throw new CustomException("Test dropped!");
        }
    }

    @Test(dataProvider
            = "File description", dataProviderClass = DataProviders.class, dependsOnMethods = "accessToWidgetsTest")
    public void createNewFolderTest(File file) {
        diskService.createNewFolder(file);
        String creationFolderName = diskService.getNameFolder(file);
        Assert.assertEquals(file.getFolderName(), creationFolderName);
    }

    @Test(dataProvider = "File description", dataProviderClass = DataProviders.class, dependsOnMethods
            = {"accessToWidgetsTest", "createNewFolderTest"})
    public void createNewWordFileTest(File file) {
        diskService.createNewWordFile(file);
        String textFromCreationWordFile = diskService.getTextFromOpenedWordFile(file);
        Assert.assertEquals(file.getText(), textFromCreationWordFile);
    }

    @Test(dataProvider = "File description", dataProviderClass = DataProviders.class, dependsOnMethods
            = {"accessToWidgetsTest", "createNewFolderTest", "createNewWordFileTest"})
    public void deleteFileTest(File file) {
        diskService.deleteFilePermanently(file);
        boolean isExistDroppedFile = mainYandexDiskPage
                .openFilesEditor().enterToFolder(file.getFolderName()).isFileExist(file.getFileName());
        Assert.assertEquals(isExistDroppedFile, false);
        isExistDroppedFile = mainYandexDiskPage.openTrash().isFileExist(file.getFileName());
        Assert.assertEquals(isExistDroppedFile, false);
    }

    private boolean isValidMark() {
        String currentWorkFolder = mainYandexDiskPage.getCurrentWorkFolder();
        return "published".equals(currentWorkFolder) || "shared".equals(currentWorkFolder);
    }

}

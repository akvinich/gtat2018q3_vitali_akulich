package com.epam.tat.tests;

import com.epam.tat.framework.bo.Account;
import com.epam.tat.framework.bo.File;
import com.epam.tat.framework.bo.WidgetMarks;
import org.testng.annotations.DataProvider;

import static org.bitbucket.dollar.Dollar.$;

public class DataProviders {

    static final String VALID_CHARACTERS = $('0', '9').join() + $('A', 'Z').join();
    static final int LENGTH_OF_FOLDER_NAME = 6;
    static final int LENGTH_OF_FILENAME = 5;

    public static String folderName = randomString(LENGTH_OF_FOLDER_NAME);

    public static String wordFileName = randomString(LENGTH_OF_FILENAME);

    private static String randomString(int length) {
        return $(VALID_CHARACTERS).shuffle().slice(length).toString();
    }

    @DataProvider(name = "Valid data for login")
    public static Object[][] validAccount() {
        return new Object[][] {
                {Account.newEntity()
                        .withEmail("akvinich-test@yandex.ru").withPassword("akvinich600").build() },

        };
    }

    @DataProvider(name = "Invalid data for login")
    public static Object[][] invalidAccount() {
        return new Object[][] {
                {Account.newEntity()
                        .withEmail("akvinich-test@yandex.ru").withPassword("12345677654321").build() },

        };
    }

    @DataProvider(name = "File description")
    public static Object[][] fileData() {
        return new Object[][] {
                {File.newEntity()
                        .withFolder(folderName)
                        .withFile(wordFileName)
                        .withText("Hello world! Yes!")
                        .build() },

        };
    }

    @DataProvider(name = "Internal widgets marks")
    public static Object[][] widgetMarksData() {
        return new Object[][] {
                {WidgetMarks.newEntity()
                        .withEdgeFolderName("journal")
                        .withEdgeAddress("journal")
                        .build()
                },
                {WidgetMarks.newEntity()
                        .withEdgeFolderName("mail")
                        .withEdgeAddress("mail")
                        .build()
                },
                {WidgetMarks.newEntity()
                        .withEdgeFolderName("recent")
                        .withEdgeAddress("recent")
                        .build()
                },
                {WidgetMarks.newEntity()
                        .withEdgeFolderName("photo")
                        .withEdgeAddress("photo")
                        .build()
                },
                {WidgetMarks.newEntity()
                        .withEdgeFolderName("trash")
                        .withEdgeAddress("trash")
                        .build()
                },
                {WidgetMarks.newEntity()
                        .withEdgeFolderName("ed")
                        .withEdgeAddress("published")
                        .build()
                },
                {WidgetMarks.newEntity()
                        .withEdgeFolderName("disk")
                        .withEdgeAddress("disk")
                        .build()
                },
        };
    }

}

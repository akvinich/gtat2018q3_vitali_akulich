package com.epam.tat.tests;

import org.testng.annotations.*;

import static com.epam.tat.framework.util.Browser.*;

public class BaseTest {

    @AfterClass
    public void closeBrowser() {
        getInstance().stopBrowser();
    }
}

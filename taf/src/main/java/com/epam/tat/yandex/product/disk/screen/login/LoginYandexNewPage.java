package com.epam.tat.yandex.product.disk.screen.login;

import com.epam.tat.yandex.product.disk.screen.app.YandexAccountPage;
import org.openqa.selenium.By;

import static com.epam.tat.framework.util.Browser.*;

public class LoginYandexNewPage extends LoginYandexPage {
    public static final By LOGIN_FIELD_LOCATOR = By.id("passp-field-login");
    public static final By PASSWORD_FIELD_LOCATOR = By.id("passp-field-passwd");
    public static final By ENTER_BUTTON_LOCATOR = By.cssSelector("button.button2");

    public LoginYandexNewPage() { }

    @Override
    public boolean isOnThisPage() {
        return getInstance().isElementPresent(NEW_FORM_LOCATOR);
    }

    @Override
    public YandexAccountPage submitLoginForm(String login, String password) {
        this.fillLoginField(LOGIN_FIELD_LOCATOR, login);
        this.clickSubmitButton(ENTER_BUTTON_LOCATOR);
        this.fillPasswordField(PASSWORD_FIELD_LOCATOR, password);
        this.clickSubmitButton(ENTER_BUTTON_LOCATOR);
        return new YandexAccountPage();
    }
}

package com.epam.tat.yandex.product.disk.steps;

import com.epam.tat.framework.bo.Account;
import com.epam.tat.yandex.product.disk.screen.app.YandexAccountPage;
import com.epam.tat.yandex.product.disk.screen.login.LoginYandexPage;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.testng.Assert;

public class LoginSteps {

    private LoginYandexPage loginYandexPage = new LoginYandexPage();
    private Account account;

    @Given("^user is on login form$")
    public void userIsOnLoginForm() {
        loginYandexPage = loginYandexPage.open();
        boolean isLoginPageOpened = loginYandexPage.open().isOnThisPage();
        Assert.assertEquals(isLoginPageOpened, true, "Yandex login page is opened: ");
    }

    @And("^user has valid credentials$")
    public void userHasValidCredentials() {
        account = Account.newEntity().withEmail("akvinich-test@yandex.ru").withPassword("akvinich600").build();
    }

    @When("^user submit login form$")
    public void userSubmitLoginForm() {
        loginYandexPage.submitLoginForm(account.getEmail(), account.getPassword());
    }

    @Then("^user is reaching mailbox$")
    public void userIsReachingMailbox() {
        Assert.assertEquals(new YandexAccountPage().isOnThisPage(), true, "Yandex account page is opened: ");
    }

    @Given("^user has invalid credentials$")
    public void userHasInvalidCredentials() {
        account = Account.newEntity().withEmail("akvinich-test@yandex.ru").withPassword("1211111").build();
    }

    @Then("^user is not reaching mailbox$")
    public void userIsNotReachingMailbox() {
        Assert.assertEquals(new YandexAccountPage().isOnThisPage(), false, "Yandex account page is opened: ");
    }
}

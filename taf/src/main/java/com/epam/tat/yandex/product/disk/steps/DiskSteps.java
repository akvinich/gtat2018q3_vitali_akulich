package com.epam.tat.yandex.product.disk.steps;

import com.epam.tat.framework.bo.Account;
import com.epam.tat.framework.bo.File;
import com.epam.tat.tests.DataProviders;
import com.epam.tat.yandex.product.disk.screen.app.MainYandexDiskPage;
import com.epam.tat.yandex.product.disk.screen.app.WordEditorPage;
import com.epam.tat.yandex.product.disk.screen.app.YandexAccountPage;
import com.epam.tat.yandex.product.disk.service.AccountService;
import com.epam.tat.yandex.product.disk.service.DiskService;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.testng.Assert;

import java.util.List;

public class DiskSteps {

    private static File file;
    private static Account account;
    private static AccountService accountService = new AccountService();
    private static MainYandexDiskPage mainYandexDiskPage;
    private static DiskService diskService = new DiskService();

    @Given("^init valid account$")
    public void initValidAccount(List<Account> accounts) {
        account = accounts.get(0);
    }

    @Given("^user (?:creates|takes) file entity$")
    public void userCreatesFileEntity() {
        if (file == null) {
            file = File.newEntity()
                    .withFolder(DataProviders.folderName)
                    .withFile(DataProviders.wordFileName)
                    .withText("Hello world! Yes!")
                    .build();
        }
    }

    @When("^user clicks point of menu with mark: (.*)$")
    public void userClicksPointOfMenuWithMark(String mark) {
        mainYandexDiskPage.clickPointOfLeftMenu(mark);
    }

    @Then("^user visits widget with server folder: (.*)$")
    public void userVisitsWidgetWithServerFolder(String folder) {
        if ("published".equals(folder)) {
            Assert.assertEquals(isValidMark(), true, "Work folder published or shared: ");
        } else {
            Assert.assertEquals(mainYandexDiskPage.getCurrentWorkFolder(), folder);
        }
    }

    @And("^user is on main yandex disk page$")
    public void userIsOnMainYandexDiskPage() {
        if (mainYandexDiskPage == null) {
            accountService.logIn(account);
            Assert.assertEquals(new YandexAccountPage().isOnThisPage(), true, "Yandex account page is opened: ");
        }
        if (!this.isOnMainYandexDiskPage()) {
            mainYandexDiskPage = this.initMainYandexDiskPage();
        }
        Assert.assertEquals(mainYandexDiskPage.isOnThisPage(), true, "Main yandex disk page is opened: ");
    }

    private boolean isValidMark() {
        String currentWorkFolder = mainYandexDiskPage.getCurrentWorkFolder();
        return "published".equals(currentWorkFolder) || "shared".equals(currentWorkFolder);
    }

    private boolean isOnMainYandexDiskPage() {
        return mainYandexDiskPage != null && mainYandexDiskPage.isOnThisPage();
    }

    private MainYandexDiskPage initMainYandexDiskPage() {
        YandexAccountPage yandexAccountPage = new YandexAccountPage();
        return yandexAccountPage.goToYandexDiskPage();
    }

    @When("^user creates new folder$")
    public void userCreatesNewFolder() {
        diskService = new DiskService(mainYandexDiskPage);
        diskService.createNewFolder(file);
    }

    @Then("^user visits new folder$")
    public void userVisitsNewFolder() {
        String folderName = diskService.getNameFolder(file);
        Assert.assertEquals(file.getFolderName(), folderName);
    }

    @When("^user creates new word file$")
    public void userCreatesNewWordFile() {
        diskService.createNewWordFile(file);
    }

    @Then("^user visits new word file$")
    public void userVisitsNewWordFile() {
        String textFromCreationWordFile = diskService.getTextFromOpenedWordFile(file);
        Assert.assertEquals(file.getText(), textFromCreationWordFile);
    }

    @When("^user deletes file permanently$")
    public void userDeletesFilePermanently() {
        diskService.deleteFilePermanently(file);
    }

    @Then("^user does not find word file in the folder$")
    public void userDoesNotFindWordFileInTheFolder() {
        boolean isExistDroppedFile = mainYandexDiskPage
                .openFilesEditor().enterToFolder(file.getFolderName()).isFileExist(file.getFileName());
        Assert.assertEquals(isExistDroppedFile, false, "File exist in the folder: ");
    }

    @And("^user does not find word file in the trash$")
    public void userDoesNotFindWordFileInTheTrash() {
        boolean isExistDroppedFile = mainYandexDiskPage.openTrash().isFileExist(file.getFileName());
        Assert.assertEquals(isExistDroppedFile, false, "File exist in the trash: ");
    }

    @And("^close word file$")
    public void closeWordFile() {
        if (new WordEditorPage().isInToFrame()) {
            new WordEditorPage().closeWordEditor();
            mainYandexDiskPage.switchToCurrentWindow();
        }
    }
}

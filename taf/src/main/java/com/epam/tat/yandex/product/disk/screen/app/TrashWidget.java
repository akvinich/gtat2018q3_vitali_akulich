package com.epam.tat.yandex.product.disk.screen.app;

import org.openqa.selenium.WebElement;

public class TrashWidget extends AbstractWidget {
    public TrashWidget(WebElement element) {
        super(element);
    }

    public WebElement getButtonClearTrash() {
        return findByCss("div.listing-head__additional-actions>button");
    }

}

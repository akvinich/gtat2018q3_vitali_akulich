package com.epam.tat.yandex.product.disk.screen.app;

import com.epam.tat.framework.exception.CustomException;
import com.epam.tat.framework.loger.Log;
import org.openqa.selenium.*;

import java.util.regex.*;

import static com.epam.tat.framework.util.Browser.getInstance;

public class MainYandexDiskPage {
    private LeftMenuWidget leftMenu;
    private FilesEditorWidget filesEditorWidget;
    private TrashWidget trash;
    private String mainWindowHandle;

    private By logoDiskLinkLocator = By.cssSelector("a.logo__link.logo__link_service[href $=disk]");
    private By introWidgetLocator = By.cssSelector("div.root__listing, div[data-key='box=boxPage']");
    private By leftMenuWidgetLocator = By.cssSelector("div.sidebar.sidebar_fixed");
    private By wordDocumentButtonLocator = By.xpath("//span[contains(@class, 'file-icon_doc')]/parent::button");
    private By newFolderButtonLocator = By.xpath("//span[contains(@class, 'file-icon_dir_plus')]/parent::button");
    private By folderNameInputLocator = By.cssSelector("form.rename-dialog__rename-form input.textinput__control");
    private By buttonNameInputLocator = By.cssSelector("div.confirmation-dialog__footer button");
    private By confirmClearTrashButtonLocator = By.cssSelector("button.b-confirmation__action_right");
    private By someElementInTrashLocator = By.xpath("//div[contains(@class,'listing-item_type')]");
    private By notificationLinkLocator = By.cssSelector("a.notifications__link");
    private By notificationTextLocator = By.cssSelector("div.notifications__text.js-message");

    public MainYandexDiskPage() {
        this.initPageElements();
    }

    public boolean isOnThisPage() {
        return getInstance().isElementPresent(logoDiskLinkLocator);
    }

    private void initPageElements() {
        WebElement introWidget = getInstance().waitForVisibilityOfElement(introWidgetLocator);
        trash = new TrashWidget(introWidget);
        filesEditorWidget = new FilesEditorWidget(introWidget);
        leftMenu = new LeftMenuWidget(getInstance().waitForVisibilityOfElement(leftMenuWidgetLocator));
    }

    public MainYandexDiskPage openFilesEditor() {
        this.initPageElements();
        return this.clickPointOfLeftMenu(leftMenu.getDisk());
    }

    public MainYandexDiskPage openTrash() {
        this.initPageElements();
        return this.clickPointOfLeftMenu(leftMenu.getTrash());
    }

    public MainYandexDiskPage clickPointOfLeftMenu(String mark) {
        this.initPageElements();
        boolean isSuccessful = getInstance().loopAttemptToClick(leftMenu.getMenuElement(mark));
        if (!isSuccessful) {
            String message = "Unsuccessful attempt to click " + mark;
            Log.error(message);
            throw new CustomException(message);
        }
        return this;
    }

    private MainYandexDiskPage clickPointOfLeftMenu(WebElement pointOfMenu) {
        this.initPageElements();
        boolean isSuccessful = getInstance().loopAttemptToClick(pointOfMenu);
        if (!isSuccessful) {
            String message = "Unsuccessful attempt to click " + pointOfMenu.getTagName();
            Log.error(message);
            throw new CustomException(message);
        }
        return this;
    }

    public String getCurrentWorkFolder() {
        Pattern p = Pattern.compile("\\w+");
        Matcher m = p.matcher(getInstance().getWrappedDriver().getCurrentUrl());
        String nameFolder = "";
        while (m.find()) {
            nameFolder = m.group();
        }
        return nameFolder;
    }

    public MainYandexDiskPage clickCreateButton() {
        this.initPageElements();
        return this.clickPointOfLeftMenu(leftMenu.getCreateButton());
    }

    public MainYandexDiskPage clickNewFolderButton() {
        getInstance().click(newFolderButtonLocator);
        return this;
    }

    public MainYandexDiskPage clickNewWordDocumentButton() {
        getInstance().initWindowHandlesParameters();
        this.mainWindowHandle = getInstance().gerCurrentWindowHandle();
        getInstance().click(wordDocumentButtonLocator);
        return this;
    }

    public MainYandexDiskPage setFolderName(String fileName) {
        WebElement nameFieldElement = getInstance().waitForVisibilityOfElement(folderNameInputLocator);
        getInstance().useActionsSendKeys(nameFieldElement, fileName);
        getInstance().click(buttonNameInputLocator);
        getInstance().waitForInvisibilityOfElement(buttonNameInputLocator);
        return this;
    }

    public WordEditorPage switchToWordEditorWindow() {
        getInstance().switchToOtherWindow();
        return new WordEditorPage();
    }

    public MainYandexDiskPage switchToCurrentWindow() {
        getInstance().getWrappedDriver().switchTo().window(mainWindowHandle);
        return this;
    }

    public MainYandexDiskPage enterToFolder(String folderName) {
        Log.info("Enter to folder: " + folderName);
        this.initPageElements();
        WebElement folder = filesEditorWidget.getFileByName(folderName);
        getInstance().useActionsDoubleClick(folder);
        return this;
    }

    public ConfirmationPage openFile(String folderName) {
        initPageElements();
        WebElement folder = filesEditorWidget.getFileByName(folderName);
        getInstance().useActionsDoubleClick(folder);
        this.switchToWordEditorWindow();
        return new ConfirmationPage();
    }

    public String getFolderName() {
        Log.info("FOLDER namer: " + filesEditorWidget.getFolderName());
        initPageElements();
        return filesEditorWidget.getFolderName();
    }

    public boolean isFileExist(String fileName) {
        initPageElements();
        return filesEditorWidget.isFileExist(fileName);
    }

    public MainYandexDiskPage deleteFile(String fileName) {
        initPageElements();
        WebElement trashElement = leftMenu.getTrash();
        WebElement fileElement  = filesEditorWidget.getFileByName(fileName);
        getInstance().dragAndDrop(fileElement, trashElement);
        getInstance().waitForVisibilityOfElement(notificationLinkLocator);
        return this;
    }

    public MainYandexDiskPage clearTrash() {
        if (getInstance().isElementPresent(someElementInTrashLocator)) {
            this.initPageElements();
            getInstance().waitForPresenceOfAllElements(someElementInTrashLocator);
            WebElement clearButton = trash.getButtonClearTrash();
            clearButton.click();
            getInstance().click(confirmClearTrashButtonLocator);
            getInstance().waitForVisibilityOfElement(notificationTextLocator);
        } else {
            String message = "Unsuccessful attempt to clear Trash!";
            Log.error(message);
            throw new CustomException(message);
        }
        return this;
    }

}

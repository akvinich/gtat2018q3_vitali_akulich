package com.epam.tat.yandex.product.disk.screen.app;

import org.openqa.selenium.WebElement;

public class LeftMenuWidget extends AbstractWidget {

    private enum LinkEdges {
        recent, disk, photo, ed, journal, mail, trash
    }

    public LeftMenuWidget(WebElement element) {
        super(element);
    }

    public WebElement getRecent() {
        return this.getMenuElement(LinkEdges.recent.toString());
    }

    public WebElement getDisk() {
        return this.getMenuElement(LinkEdges.disk.toString());
    }

    public WebElement getPhoto() {
        return this.getMenuElement(LinkEdges.photo.toString());
    }

    public WebElement getPublished() {
        return this.getMenuElement(LinkEdges.ed.toString());
    }

    public WebElement getJournal() {
        return this.getMenuElement(LinkEdges.journal.toString());
    }

    public WebElement getMail() {
        return this.getMenuElement(LinkEdges.mail.toString());
    }

    public WebElement getTrash() {
        return this.getMenuElement(LinkEdges.trash.toString());
    }

    public WebElement getCreateButton() {
        return this.findByCss("div.sidebar__buttons button");
    }

    public WebElement getMenuElement(String edges) {
        return this.findByCss("a[href $=" + edges + "]");
    }

}

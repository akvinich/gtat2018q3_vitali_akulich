package com.epam.tat.yandex.product.disk.screen.app;

import org.openqa.selenium.WebElement;

public class AbstractWidget extends AbstractSearchContext<WebElement> {
    public AbstractWidget(WebElement element) {
        super(element);
    }

    protected boolean isElementPresent(String xpath) {
        return this.findAllByXPath(xpath).size() > 0;
    }
}

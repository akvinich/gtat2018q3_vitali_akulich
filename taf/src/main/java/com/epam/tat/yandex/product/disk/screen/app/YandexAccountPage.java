package com.epam.tat.yandex.product.disk.screen.app;

import org.openqa.selenium.By;

import static com.epam.tat.framework.util.Browser.*;

public class YandexAccountPage {
    By fullEmailName = By.cssSelector("span.user-account__subname");
    By emailHeader = By.cssSelector("div.dheader-user a");
    By myServicesItemOfMainMenu = By.cssSelector("a.dheader-navigation-item[href $=services]");
    By linkToYandexDiskPage = By.cssSelector("div.dashboard__section.cloud>div>a");

    public YandexAccountPage() { }

    public boolean isOnThisPage() {
        return getInstance().isElementPresent(emailHeader);
    }

    public String getEmailName() {
        getInstance().click(emailHeader);
        return getInstance().getText(fullEmailName);
    }

    public MainYandexDiskPage goToYandexDiskPage() {
        getInstance().click(myServicesItemOfMainMenu);
        getInstance().click(linkToYandexDiskPage);
        return new MainYandexDiskPage();
    }
}

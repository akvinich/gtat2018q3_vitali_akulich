package com.epam.tat.yandex.product.disk.service;

import com.epam.tat.framework.bo.File;
import com.epam.tat.framework.bo.WidgetMarks;
import com.epam.tat.framework.exception.CustomException;
import com.epam.tat.framework.loger.Log;
import com.epam.tat.yandex.product.disk.screen.app.MainYandexDiskPage;
import com.epam.tat.yandex.product.disk.screen.app.WordEditorPage;
import com.epam.tat.yandex.product.disk.screen.app.YandexAccountPage;

public class DiskService {
    private static MainYandexDiskPage mainYandexDiskPage;

    public DiskService() {

    }

    public DiskService(MainYandexDiskPage page) {
        mainYandexDiskPage = page;
    }

    public void visitWidget(WidgetMarks marks) {
        Log.info("Visit widget with mark: " + marks.getEdgeFolderName());
        if (!this.isOnMainYandexDiskPage()) {
            mainYandexDiskPage = this.initMainYandexDiskPage();
        }
        if (mainYandexDiskPage.isOnThisPage()) {
            this.goToPointOfMenu(marks.getEdgeFolderName());
        } else {
            String message = "Filed to visit widget!";
            Log.error(message);
            throw new CustomException(message);
        }
    }

    public void createNewFolder(File file) {
        Log.info("Create new folder with name: " + file.getFolderName());
        if (!this.isOnMainYandexDiskPage()) {
            mainYandexDiskPage = this.initMainYandexDiskPage();
        }
        if (mainYandexDiskPage.isOnThisPage()) {
            mainYandexDiskPage.openFilesEditor().clickCreateButton().clickNewFolderButton()
                    .setFolderName(file.getFolderName());
        } else {
            String message = "Filed to create folder!";
            Log.error(message);
            throw new CustomException(message);
        }
    }

    public String getNameFolder(File file) {
        Log.info("Get header (h1) in folder: " + file.getFolderName());
        if (!this.isOnMainYandexDiskPage()) {
            mainYandexDiskPage = this.initMainYandexDiskPage();
        }
        if (mainYandexDiskPage.isOnThisPage()) {
            return mainYandexDiskPage.openFilesEditor().enterToFolder(file.getFolderName()).getFolderName();
        } else {
            String message = "Filed to get header of folder!";
            Log.error(message);
            throw new CustomException(message);
        }
    }

    public void createNewWordFile(File file) {
        Log.info("Create new word file with name: " + file.getFileName());
        if (!this.isOnMainYandexDiskPage()) {
            mainYandexDiskPage = this.initMainYandexDiskPage();
        }
        if (mainYandexDiskPage.isOnThisPage()) {
            WordEditorPage wordEditorPage = mainYandexDiskPage
                    .openFilesEditor().enterToFolder(file.getFolderName())
                    .clickCreateButton().clickNewWordDocumentButton().switchToWordEditorWindow();
            if (wordEditorPage.isOnThisPage()) {
                wordEditorPage.switchToEditor()
                        .setFileName(file.getFileName()).typeText(file.getText());
                wordEditorPage.closeWordEditor();
                mainYandexDiskPage.switchToCurrentWindow();
            }
        } else {
            String message = "Filed to create new word file!";
            Log.error(message);
            throw new CustomException(message);
        }
    }

    public void deleteFilePermanently(File file) {
        Log.info("Delete word file permanently: " + file.getFileName());
        if (new WordEditorPage().isInToFrame()) {
            new WordEditorPage().closeWordEditor();
            mainYandexDiskPage.switchToCurrentWindow();
        }
        if (!this.isOnMainYandexDiskPage()) {
            mainYandexDiskPage = this.initMainYandexDiskPage();
        }
        if (mainYandexDiskPage.isOnThisPage()) {
            mainYandexDiskPage.openFilesEditor().enterToFolder(file.getFolderName()).deleteFile(file.getFileName())
                    .openTrash().clearTrash();
        } else {
            String message = "Filed to delete file!";
            Log.error(message);
            throw new CustomException(message);
        }
    }

    public String getTextFromOpenedWordFile(File file) {
        Log.info("Get text from word file: " + file.getFileName());
        if (!this.isOnMainYandexDiskPage()) {
            mainYandexDiskPage = this.initMainYandexDiskPage();
        }
        if (mainYandexDiskPage.isOnThisPage()) {
            return mainYandexDiskPage.openFilesEditor().enterToFolder(file.getFolderName()).openFile(file.getFileName())
                    .confirm().switchToEditor().getText();
        } else {
            String message = "Filed to get text from word file!";
            Log.error(message);
            throw new CustomException(message);
        }
    }

    private boolean isOnMainYandexDiskPage() {
        return mainYandexDiskPage != null && mainYandexDiskPage.isOnThisPage();
    }

    private void goToPointOfMenu(String mark) {
        mainYandexDiskPage.clickPointOfLeftMenu(mark);
    }

    private MainYandexDiskPage initMainYandexDiskPage() {
        YandexAccountPage yandexAccountPage = new YandexAccountPage();
        return yandexAccountPage.goToYandexDiskPage();
    }
}

package com.epam.tat.yandex.product.disk.steps;

import cucumber.api.java.en.Then;

import static com.epam.tat.framework.util.Browser.getInstance;

public class Hooks {

    @Then("^user stops browser$")
    public void stopBrowser() {
        getInstance().stopBrowser();
    }
}

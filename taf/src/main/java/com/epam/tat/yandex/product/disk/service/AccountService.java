package com.epam.tat.yandex.product.disk.service;

import com.epam.tat.framework.bo.Account;
import com.epam.tat.framework.loger.Log;
import com.epam.tat.yandex.product.disk.screen.login.LoginYandexPage;

public class AccountService {

    public void logIn(Account account) {
        Log.info("Authorization with login: " + account.getEmail() + " password " + account.getPassword());
        LoginYandexPage loginYandexPage = new LoginYandexPage();
        loginYandexPage = loginYandexPage.open();
        if (loginYandexPage.isOnThisPage()) {
            loginYandexPage.submitLoginForm(account.getEmail(), account.getPassword());
        }
    }
}

package com.epam.tat.yandex.product.disk.screen.app;

import org.openqa.selenium.WebElement;

public class FilesEditorWidget extends AbstractWidget {

    public FilesEditorWidget(WebElement element) {
        super(element);
    }

    public WebElement getFileByName(String fileName) {
        return findByXPath("//div[contains(@class, 'listing-item_type')][.//span[contains(.,'"
                + fileName + "')]]");
    }

    public String getFolderName() {
        return findByCss("div.listing-head__heading-wrapper h1")
                .getText();
    }

    public boolean isFileExist(String fileName) {
        return isElementPresent("//div[contains(@class, 'listing-item_type')][.//span[contains(.,'"
                + fileName + "')]]");
    }
}

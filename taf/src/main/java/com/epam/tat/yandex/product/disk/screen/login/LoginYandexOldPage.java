package com.epam.tat.yandex.product.disk.screen.login;

import com.epam.tat.yandex.product.disk.screen.app.YandexAccountPage;
import org.openqa.selenium.By;

import static com.epam.tat.framework.util.Browser.*;

public class LoginYandexOldPage extends LoginYandexPage {
    public static final By LOGIN_FIELD_LOCATOR
            = By.cssSelector("div.passport-Domik input[autocomplete='username']");
    public static final By PASSWORD_FIELD_LOCATOR
            = By.cssSelector("div.passport-Domik input[autocomplete='current-password']");
    public static final By ENTER_BUTTON_LOCATOR
            = By.cssSelector("div.passport-Domik button.passport-Button[type='submit']");

    public LoginYandexOldPage() { }

    @Override
    public boolean isOnThisPage() {
        return getInstance().isElementPresent(OLD_FORM_LOCATOR);
    }

    public YandexAccountPage submitLoginForm(String login, String password) {
        this.fillLoginField(LOGIN_FIELD_LOCATOR, login);
        this.prepareLoginPageElement(PASSWORD_FIELD_LOCATOR).clear();
        this.prepareLoginPageElement(PASSWORD_FIELD_LOCATOR).sendKeys(password);
        this.clickSubmitButton(ENTER_BUTTON_LOCATOR);
        return new YandexAccountPage();
    }
}

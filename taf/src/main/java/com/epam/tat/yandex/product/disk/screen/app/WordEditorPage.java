package com.epam.tat.yandex.product.disk.screen.app;

import org.openqa.selenium.*;

import static com.epam.tat.framework.util.Browser.*;

public class WordEditorPage {
    By frameLocator = By.cssSelector("iframe.editor-doc__iframe");
    By userNameLocator = By.cssSelector("span.UsernameNameContainer");
    By documentNameLocator = By.id("BreadcrumbTitle");
    By textAreaLocator = By.cssSelector("div.WordLastSectionBackground");
    By textLocator = By.cssSelector("p.Paragraph");
    By saveOnDiskSpanLocator = By.id("BreadcrumbSaveStatus");

    private final int timeoutSeconds = 30;

    public WordEditorPage() { }

    public boolean isOnThisPage() {
        return getInstance().isElementPresent(frameLocator);
    }

    public boolean isInToFrame() {
        return getInstance().isElementPresent(documentNameLocator);
    }

    public WordEditorPage switchToEditor() {
        getInstance().switchToFrame(frameLocator);
        getInstance().waitForVisibilityOfElement(userNameLocator);
        return  this;
    }

    public WordEditorPage setFileName(String fileName) {
        getInstance().waitForVisibilityOfElement(saveOnDiskSpanLocator);
        WebElement docName = getInstance().waitForVisibilityOfElement(documentNameLocator);
        getInstance().click(documentNameLocator);
        getInstance().useActionsDoubleClick(docName);
        getInstance().waitForAttributeContains(docName, "contenteditable", "true");
        getInstance().pressBackSpace(documentNameLocator);
        getInstance().sendKeys(documentNameLocator, fileName);
        getInstance().pressEnter(documentNameLocator);
        return this;
    }

    public WordEditorPage typeText(String text) {
        String waitText = getInstance().getText(saveOnDiskSpanLocator).trim();
        WebElement docText = getInstance().waitForVisibilityOfElement(textAreaLocator);
        getInstance().useActionsClick(docText);
        getInstance().useActionsSendKeys(docText, text);
        By saveMarkLocator = By.xpath("//span[@id='BreadcrumbSaveStatus'][contains(., '" + waitText + "')]");
        getInstance().waitForVisibilityOfElement(saveMarkLocator, timeoutSeconds);
        return this;
    }

    public void  closeWordEditor() {
        getInstance().closeWindow();
    }

    public String getText() {
        WebElement text = getInstance().waitForPresenceOfElement(textLocator);
        return text.getText().trim();
    }

}

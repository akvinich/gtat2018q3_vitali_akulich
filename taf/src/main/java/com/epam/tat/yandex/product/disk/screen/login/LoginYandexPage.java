package com.epam.tat.yandex.product.disk.screen.login;

import com.epam.tat.yandex.product.disk.screen.app.YandexAccountPage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import static com.epam.tat.framework.util.Browser.*;

public class LoginYandexPage {

    public static final String URL = "https://passport.yandex.ru";

    public static final By NEW_FORM_LOCATOR = By.cssSelector("div.passp-page-overlay");
    public static final By OLD_FORM_LOCATOR = By.cssSelector("div.passport-Domik");

    public LoginYandexPage() { }

    public boolean isOnThisPage() {
        return false;
    }

    public LoginYandexPage open() {
        getInstance().get(URL);
        if (getInstance().isElementPresent(NEW_FORM_LOCATOR)) {
            return new LoginYandexNewPage();
        } else if (getInstance().isElementPresent(OLD_FORM_LOCATOR)) {
            return new LoginYandexOldPage();
        }
        return null;
    }

    protected WebElement prepareLoginPageElement(By by) {
        return getInstance().waitForVisibilityOfElement(by);
    }

    protected void fillLoginField(By loginField, String text) {
        getInstance().useActionsSendKeys(prepareLoginPageElement(loginField), text);
    }

    protected void fillPasswordField(By passwordField, String text) {
        getInstance().useActionsSendKeys(prepareLoginPageElement(passwordField), text);
    }

    protected void clickSubmitButton(By buttonPassword) {
        getInstance().click(buttonPassword);
    }

    public YandexAccountPage submitLoginForm(String login, String password) {
        return new YandexAccountPage();
    }
}

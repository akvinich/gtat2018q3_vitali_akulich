package com.epam.tat.yandex.product.disk.screen.app;

import org.openqa.selenium.*;

import static com.epam.tat.framework.util.Browser.*;

public class ConfirmationPage {
    By editButtonLocator = By.cssSelector("div.hover-tooltip__tooltip-anchor>a.button2");

    protected ConfirmationPage() { }

    public boolean isOnThisPage() {
        return getInstance().isElementPresent(editButtonLocator);
    }

    public WordEditorPage confirm() {
        getInstance().click(editButtonLocator);
        return new WordEditorPage();
    }
}

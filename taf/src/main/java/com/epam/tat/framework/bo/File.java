package com.epam.tat.framework.bo;

public class File {

    private String folderName;
    private String fileName;
    private String text;

    public String getFolderName() {
        return folderName;
    }

    public void setFolderName(String folderName) {
        this.folderName = folderName;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public static Builder newEntity() {
        return new File().new Builder();
    }

    public final class Builder {

        private Builder() { }

        public Builder withFolder(String folderNameBuild) {
            File.this.folderName = folderNameBuild;
            return this;
        }

        public Builder withFile(String fileNameBuild) {
            File.this.fileName = fileNameBuild;
            return this;
        }

        public Builder withText(String textBuild) {
            File.this.text = textBuild;
            return this;
        }

        public File build() {
            return File.this;
        }
    }
}

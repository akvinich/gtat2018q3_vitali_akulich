package com.epam.tat.framework.bo;

public class WidgetMarks {

    private String edgeFolderName;
    private String edgeAddress;

    public String getEdgeFolderName() {
        return edgeFolderName;
    }

    public void setEdgeFolderName(String edgeFolderName) {
        this.edgeFolderName = edgeFolderName;
    }

    public String getEdgeAddress() {
        return edgeAddress;
    }

    public void setEdgeAddress(String edgeAddress) {
        this.edgeAddress = edgeAddress;
    }

    public static Builder newEntity() {
        return new WidgetMarks().new Builder();
    }

    public final class Builder {

        private Builder() { }

        public Builder withEdgeFolderName(String edgeFolderNameBuild) {
            WidgetMarks.this.edgeFolderName = edgeFolderNameBuild;
            return this;
        }

        public Builder withEdgeAddress(String edgeAddressBuild) {
            WidgetMarks.this.edgeAddress = edgeAddressBuild;
            return this;
        }

        public WidgetMarks build() {
            return WidgetMarks.this;
        }
    }

}

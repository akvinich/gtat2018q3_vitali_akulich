package com.epam.tat.framework.util;

import com.epam.tat.framework.exception.CustomException;
import com.epam.tat.framework.loger.Log;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.concurrent.TimeUnit;

public final class Browser implements WrapsDriver {

    public static final int PRESENT_ELEMENT_TIME_OUT = 5;
    public static final int DEFAULT_TIMEOUT_SECONDS = 10;

    private static ThreadLocal<Browser> instance = new ThreadLocal<>();

    private WebDriver wrappedWebDriver;

    private String mainWindowHandle;
    private Set<String> oldWindows;

    private Browser() {
        wrappedWebDriver = BrowserFactory.getBrowser();
    }

    public static synchronized Browser getInstance() {
        if (instance.get() == null) {
            instance.set(new Browser());
        }
        return instance.get();
    }

    @Override
    public WebDriver getWrappedDriver() {
        return wrappedWebDriver;
    }

    public void get(String url) {
        wrappedWebDriver.navigate().to(url);
    }

    public void stopBrowser() {
        try {
            if (getWrappedDriver() != null) {
                getWrappedDriver().quit();
            }
        } catch (WebDriverException e) {
            e.printStackTrace();
        } finally {
            instance.set(null);
        }
    }

    public boolean isElementPresent(By by) {
        try {
            wrappedWebDriver.manage().timeouts().implicitlyWait(PRESENT_ELEMENT_TIME_OUT, TimeUnit.SECONDS);
            return wrappedWebDriver.findElements(by).size() > 0;
        } finally {
            wrappedWebDriver.manage().timeouts().implicitlyWait(0, TimeUnit.SECONDS);
        }
    }

    public void switchToOtherWindow() {
        String newWindow = this.getDefaultWait().until(thereIsWindowOtherThan(oldWindows));
        wrappedWebDriver.switchTo().window(newWindow);
    }

    private ExpectedCondition<String> thereIsWindowOtherThan(Set<String> windows) {
        return input -> {
            Set<String> handles = input.getWindowHandles();
            handles.removeAll(windows);
            return handles.size() > 0 ? handles.iterator().next() : null;
        };
    }

    private WebDriverWait getDefaultWait() {
        return new WebDriverWait(wrappedWebDriver, DEFAULT_TIMEOUT_SECONDS);
    }

    public void initWindowHandlesParameters() {
        this.mainWindowHandle = wrappedWebDriver.getWindowHandle();
        this.oldWindows = wrappedWebDriver.getWindowHandles();
    }

    public Set<String> getWindowHandles() {
        return wrappedWebDriver.getWindowHandles();
    }

    public String gerCurrentWindowHandle() {
        return wrappedWebDriver.getWindowHandle();
    }

    public boolean loopAttemptToClick(WebElement element) {
        Log.debug("Loop click " + element.getTagName());
        this.highlightElement(element);
        return this.getDefaultWait().until(new ExpectedCondition<Boolean>() {
            public Boolean apply(WebDriver webDriver) {
                try {
                    element.click();
                } catch (StaleElementReferenceException e) {
                    return false;
                }
                return true;
            }
        });
    }

    public String getText(By by) {
        return waitForVisibilityOfElement(by).getText();
    }

    public void click(By by) {
        Log.debug("Click " + by);
        this.highlightElement(waitForVisibilityOfElement(by));
        wrappedWebDriver.findElement(by).click();
    }

    public void useActionsClick(WebElement element) {
        Log.debug("Click with actions " + element.getTagName());
        this.highlightElement(element);
        new Actions(wrappedWebDriver).moveToElement(element).click().build().perform();
    }

    public void useActionsDoubleClick(WebElement element) {
        Log.debug("Double click with actions " + element.getTagName());
        this.highlightElement(element);
        new Actions(wrappedWebDriver).moveToElement(element).doubleClick().build().perform();
    }

    public void useActionsSendKeys(WebElement element, String text) {
        Log.debug("Type with actions " + element.getTagName());
        this.highlightElement(element);
        new Actions(wrappedWebDriver).moveToElement(element).sendKeys(text).build().perform();
    }

    public void dragAndDrop(WebElement drag, WebElement drop) {
        Log.debug("Drag and drop from " + drag.getTagName() + " to " + drop.getTagName());
        this.highlightElement(drag);
        new Actions(wrappedWebDriver).dragAndDrop(drag, drop).build().perform();
        this.highlightElement(drop);
    }

    public void pressBackSpace(By by) {
        waitForVisibilityOfElement(by).sendKeys(Keys.BACK_SPACE);
    }

    public void clear(By by) {
        Log.debug("Clear " + by);
        waitForVisibilityOfElement(by).clear();
    }

    public void sendKeys(By by, String text) {
        Log.debug("Type " + by);
        waitForVisibilityOfElement(by).sendKeys(Keys.chord(Keys.CONTROL, "a"), text);
    }

    public void pressEnter(By by) {
        Log.debug("Press ENTER " + by);
        waitForVisibilityOfElement(by).sendKeys(Keys.ENTER);
    }

    public WebElement waitForVisibilityOfElement(By by, long timeout) {
        return new WebDriverWait(wrappedWebDriver, timeout)
                .until(ExpectedConditions.visibilityOfElementLocated(by));
    }

    public WebElement waitForVisibilityOfElement(By by) {
        return this.waitForVisibilityOfElement(by, DEFAULT_TIMEOUT_SECONDS);
    }

    public WebElement waitForPresenceOfElement(By by) {
        return new WebDriverWait(wrappedWebDriver, DEFAULT_TIMEOUT_SECONDS)
                .until(ExpectedConditions.presenceOfElementLocated(by));
    }

    public List<WebElement> waitForPresenceOfAllElements(By by) {
        return new WebDriverWait(wrappedWebDriver, DEFAULT_TIMEOUT_SECONDS)
                .until(ExpectedConditions.presenceOfAllElementsLocatedBy(by));
    }

    public boolean waitForInvisibilityOfElement(By by, long timeout) {
        return new WebDriverWait(wrappedWebDriver, timeout)
                .until(ExpectedConditions.invisibilityOfElementLocated(by));
    }

    public boolean waitForInvisibilityOfElement(By by) {
        return waitForInvisibilityOfElement(by, DEFAULT_TIMEOUT_SECONDS);
    }

    public boolean waitForAttributeContains(WebElement element, String attribute, String attributeValue) {
        return new WebDriverWait(wrappedWebDriver, DEFAULT_TIMEOUT_SECONDS)
                .until(ExpectedConditions.attributeContains(element, attribute, attributeValue));
    }

    public void switchToFrame(By by) {
        Log.debug("Switch to frame " + by);
        this.getDefaultWait().until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(by)).switchTo();
    }

    public void closeWindow() {
        Log.debug("Close Window!");
        wrappedWebDriver.close();
    }

    private void highlightElement(WebElement element) {
        String backgroundColor = element.getCssValue("backgroundColor");
        JavascriptExecutor js = (JavascriptExecutor) wrappedWebDriver;
        js.executeScript("arguments[0].style.backgroundColor = '" + "yellow" + "'", element);
        this.makeScreenshot();
        js.executeScript("arguments[0].style.backgroundColor = '" + backgroundColor + "'", element);
    }

    private void makeScreenshot() {
        Path path = Paths.get("logs/screenshots/" + System.nanoTime() + "screenshot.png");
        File screenshotFile = new File(path.toString());
        File screenshotAsFile = ((TakesScreenshot) wrappedWebDriver).getScreenshotAs(OutputType.FILE);
        try {
            FileUtils.copyFile(screenshotAsFile, screenshotFile);
            Log.info("Screenshot taken: file: " + path.toString());
        } catch (IOException e) {
            Log.error("Failed to copy file " + e.getMessage());
            throw new CustomException("Failed to copy file");
        }
    }

}

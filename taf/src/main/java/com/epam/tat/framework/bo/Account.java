package com.epam.tat.framework.bo;

public class Account {

    String email;
    String password;

    public static Builder newEntity() {
        return new Account().new Builder();
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public final class Builder {

        private Builder() { }

        public Builder withEmail(String emailBuild) {
            Account.this.setEmail(emailBuild);
            return this;
        }

        public Builder withPassword(String passwordBuild) {
            Account.this.setPassword(passwordBuild);
            return this;
        }

        public Account build() {
            return Account.this;
        }
    }
}

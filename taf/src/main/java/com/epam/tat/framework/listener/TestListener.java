package com.epam.tat.framework.listener;

import org.testng.*;

public class TestListener implements ITestListener, IInvokedMethodListener {

    private static final int SUCCESS_STATUS = 1;
    private static final int FAILED_STATUS = 2;
    private static final int SKIPPED_STATUS = 3;

    public void beforeInvocation(IInvokedMethod iInvokedMethod, ITestResult iTestResult) {
        System.out.println("[METHOD_STARTED] - " + iInvokedMethod.getTestMethod().getMethodName());
    }

    public void afterInvocation(IInvokedMethod iInvokedMethod, ITestResult iTestResult) {
        switch (iTestResult.getStatus()) {
            case SUCCESS_STATUS:
                System.out.println("[TEST_SUCCESS] - " + iInvokedMethod.getTestMethod().getMethodName());
                break;
            case FAILED_STATUS:
                System.out.println("[TEST_FAILED] - " + iInvokedMethod.getTestMethod().getMethodName());
                break;
            case SKIPPED_STATUS:
                System.out.println("[TEST_SKIPPED] - " + iInvokedMethod.getTestMethod().getMethodName());
                break;
            default:
                throw new RuntimeException("Not supported status " + iTestResult.getStatus());
        }
    }
}

package com.epam.tat.framework.runner;

import com.beust.jcommander.IStringConverter;
import com.beust.jcommander.Parameter;
import com.beust.jcommander.converters.IParameterSplitter;
import com.epam.tat.framework.util.BrowserType;
import org.testng.xml.XmlSuite;

import java.util.Arrays;
import java.util.List;

public final class Parameters {

    public static final int PORT = 4444;

    private static Parameters instance;

    @Parameter(names = {"--help"}, help = false, description = "Help")
    private boolean help;

    @Parameter(names = {"--browser", "-b"}, description = "Browser type",
            converter = BrowserTypeConverter.class, required = true)
    private BrowserType browserType = BrowserType.CHROME;

    @Parameter(names = {"--gecko", "-g"}, description = "Path to geckoDriver")
    private String geckoDriver = "./src/main/resources/geckodriver.exe";

    @Parameter(names = {"--chrome", "-ch"}, description = "Path to chromeDriver")
    private String chromeDriver = "./src/main/resources/chromedriver.exe";

    @Parameter(names = {"--log", "-l"}, description = "Path to log config")
    private String logConfig = "./src/main/resources/log4j.properties";

    @Parameter(names = {"--host"}, description = "Path to log config")
    private String gridHost = "http://localhost";

    @Parameter(names = {"--port"}, description = "Path to log config")
    private int gridPort = PORT;

    @Parameter(names = {"--suites", "-s"}, description = "Path to test suites", splitter = SuitesSplitter.class)
    private List<String> testConfig = Arrays.asList(new String[]{"./src/main/resources/testng.xml"});

    @Parameter(names = {"--parallel", "-p"}, description = "Parallel entity",
            converter = XmlSuiteParallelModeConverter.class, required = true)
    private XmlSuite.ParallelMode parallelEntity = XmlSuite.ParallelMode.TESTS;

    @Parameter(names = {"--threads", "-t"}, description = "Count of threads", required = true)
    private int threadsCount = 2;

    private Parameters() {
    }

    public boolean isHelp() {
        return help;
    }

    public BrowserType getBrowserType() {
        return browserType;
    }

    public String getGridHost() {
        return gridHost;
    }

    public int getGridPort() {
        return gridPort;
    }

    public String getGeckoDriver() {
        return geckoDriver;
    }

    public String getChromeDriver() {
        return chromeDriver;
    }

    public List<String> getTestConfig() {
        return testConfig;
    }

    public String getLogConfig() {
        return logConfig;
    }

    public XmlSuite.ParallelMode getParallelEntity() {
        return parallelEntity;
    }

    public int getThreadsCount() {
        return threadsCount;
    }

    public static synchronized Parameters instance() {
        if (instance == null) {
            instance = new Parameters();
        }
        return instance;
    }

    public static class SuitesSplitter implements IParameterSplitter {
        public List<String> split(String value) {
            return Arrays.asList(value.split(";"));
        }
    }

    public static class BrowserTypeConverter implements IStringConverter<BrowserType> {
        @Override
        public BrowserType convert(String s) {
            return BrowserType.valueOf(s.toUpperCase());
        }
    }

    public static class XmlSuiteParallelModeConverter implements IStringConverter<XmlSuite.ParallelMode> {
        @Override
        public XmlSuite.ParallelMode convert(String s) {
            return XmlSuite.ParallelMode.valueOf(s.toUpperCase());
        }
    }

}

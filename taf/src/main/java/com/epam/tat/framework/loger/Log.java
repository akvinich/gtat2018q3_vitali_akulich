package com.epam.tat.framework.loger;

import com.beust.jcommander.JCommander;
import com.epam.tat.framework.runner.Parameters;
import org.apache.log4j.*;

import java.io.FileReader;
import java.io.IOException;

public class Log {

    private static Logger logger = Logger.getLogger("Custom LOGGER: ");

    public static void configure(String parseInput) {
        try (FileReader fileLog = new FileReader(parseInput)) {
            PropertyConfigurator.configure(parseInput);
        } catch (IOException e) {
            System.out.println("Incorrect path to log4j.properties file: " + e.getMessage());
            new JCommander(Parameters.instance()).usage();
            System.exit(1);
        }
    }

    public static void debug(String message) {
        logger.debug(message);
    }

    public static void info(String message) {
        logger.info(message);
    }

    public static void error(String message) {
        logger.error(message);
    }

}

package com.epam.tat.framework.loger;

import org.apache.log4j.AppenderSkeleton;
import org.apache.log4j.Layout;
import org.apache.log4j.spi.LoggingEvent;
import org.testng.Reporter;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class TestNGReportAppender extends AppenderSkeleton {

    public static final String SCR_TAKEN_LOG_MESSAGE = "Screenshot taken\\: file\\: ";
    private final Pattern p = Pattern.compile(SCR_TAKEN_LOG_MESSAGE);

    public TestNGReportAppender() {
    }

    @Override
    protected void append(final LoggingEvent event) {
        String eventMessage = this.eventToString(event);
        if (this.isLogScreenshotTaken(eventMessage)) {
            eventMessage = this.makeLink(eventMessage);
        }
        Reporter.log(eventMessage);
    }

    private boolean isLogScreenshotTaken(String logMessage) {
        Matcher m = p.matcher(logMessage);
        return m.find();
    }

    private String makeLink(String logMessage) {
        String href = p.split(logMessage)[1];
        Pattern p1 = Pattern.compile("(\\w)+\\.png");
        Matcher m1 = p1.matcher(logMessage);
        String nameScr = "src";
        while (m1.find()) {
            nameScr = m1.group();
        }
        return SCR_TAKEN_LOG_MESSAGE + "<a href=\"" + href + "\" target=\"blank\">" + nameScr + "</a>";
    }

    private String eventToString(final LoggingEvent event) {
        final StringBuilder result = new StringBuilder(layout.format(event));

        if (layout.ignoresThrowable()) {
            final String[] s = event.getThrowableStrRep();
            if (s != null) {
                for (final String value : s) {
                    result.append(value).append(Layout.LINE_SEP);
                }
            }
        }
        return result.toString();
    }

    @Override
    public void close() {

    }

    @Override
    public boolean requiresLayout() {
        return true;
    }

}

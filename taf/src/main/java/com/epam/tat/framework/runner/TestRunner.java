package com.epam.tat.framework.runner;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.ParameterException;
import com.epam.tat.framework.listener.SuiteListener;
import com.epam.tat.framework.listener.TestListener;
import com.epam.tat.framework.loger.Log;
import org.apache.commons.io.FileUtils;
import org.testng.TestNG;

import java.io.File;
import java.io.IOException;

public class TestRunner {

    public static TestNG createTestNG() {
        TestNG testNG = new TestNG();
        testNG.addListener(new TestListener());
        testNG.addListener(new SuiteListener());
        testNG.setTestSuites(Parameters.instance().getTestConfig());
        testNG.setParallel(Parameters.instance().getParallelEntity());
        testNG.setThreadCount(Parameters.instance().getThreadsCount());
        return testNG;
    }

    public static void main(String[] args) {
        System.out.println("Parse cli");
        TestRunner.parseCli(args);
        TestRunner.log4jConfigure();
        Log.info("Start app");
        TestRunner.createTestNG().run();
        TestRunner.copyLogsDirectory();
        Log.info("Finish app");
    }

    private static void log4jConfigure() {
        Log.configure(Parameters.instance().getLogConfig());
    }

    private static void parseCli(String[] args) {
        System.out.println("Parse clis using JCommander");
        JCommander jCommander = new JCommander(Parameters.instance());
        try {
            jCommander.parse(args);
        } catch (ParameterException e) {
            Log.error(e.getMessage());
            jCommander.usage();
            System.exit(1);
        } catch (IllegalArgumentException e) {
            System.out.println("Incorrect data inputs: " + e.getMessage());
            jCommander.usage();
            System.exit(1);
        }
        if (Parameters.instance().isHelp()) {
            jCommander.usage();
            System.exit(0);
        }
    }

    private static void copyLogsDirectory() {
        Log.info("Copy screenshots in test-output folder");
        File source = new File("./logs");
        File dest = new File("./test-output/logs");
        try {
            FileUtils.copyDirectory(source, dest);
        } catch (IOException e) {
            Log.error(e.getMessage());
        }
    }
}

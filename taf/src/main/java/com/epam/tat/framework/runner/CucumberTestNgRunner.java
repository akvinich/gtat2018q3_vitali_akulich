package com.epam.tat.framework.runner;

import cucumber.api.CucumberOptions;
import cucumber.api.testng.AbstractTestNGCucumberTests;

@CucumberOptions(
        strict = true,
        features = "src/main/resources/features",
        glue = "com.epam.tat.yandex.product.disk.steps",
        tags = "@all",
        plugin = {"json:target/cucumber-report.json", "html:target/cucumber-report"}
)
public class CucumberTestNgRunner extends AbstractTestNGCucumberTests {

}

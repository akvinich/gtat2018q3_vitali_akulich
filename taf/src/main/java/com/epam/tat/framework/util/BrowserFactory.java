package com.epam.tat.framework.util;

import com.epam.tat.framework.exception.NotSupportedBrowserException;
import com.epam.tat.framework.listener.WebEventListener;
import com.epam.tat.framework.runner.Parameters;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

public final class BrowserFactory {

    private BrowserFactory() { }

    public static WebDriver getBrowser() {
        DesiredCapabilities desiredCapabilities;
        switch (Parameters.instance().getBrowserType()) {
            case CHROME:
                desiredCapabilities = DesiredCapabilities.chrome();
                break;
            case FIREFOX:
                desiredCapabilities = DesiredCapabilities.firefox();
                break;
            default:
                throw new NotSupportedBrowserException("Not supported browser "
                        + Parameters.instance().getBrowserType());
        }
        return configureWebDriver(desiredCapabilities);
    }

    private static WebDriver configureWebDriver(DesiredCapabilities desiredCapabilities) {
        WebDriver webDriver = null;
        try {
            webDriver = new RemoteWebDriver(new URL(Parameters.instance().getGridHost() + ":"
                    + Parameters.instance().getGridPort() + "/wd/hub"), desiredCapabilities);
        } catch (MalformedURLException e) {
            throw new RuntimeException("Unable to run Chrome via Hub");
        }
        EventFiringWebDriver eventFiringWebDriver = new EventFiringWebDriver(webDriver);
        eventFiringWebDriver.register(new WebEventListener());
        eventFiringWebDriver.manage().timeouts().implicitlyWait(0, TimeUnit.SECONDS);
        eventFiringWebDriver.manage().deleteAllCookies();
        eventFiringWebDriver.manage().window().maximize();
        return eventFiringWebDriver;
    }
}

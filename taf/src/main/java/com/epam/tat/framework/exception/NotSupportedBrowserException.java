package com.epam.tat.framework.exception;

public class NotSupportedBrowserException extends RuntimeException {

    public NotSupportedBrowserException(String message) {
        super(message);
    }
}

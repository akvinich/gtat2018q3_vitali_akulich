package com.epam.tat.framework.listener;

import com.epam.tat.framework.loger.Log;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.events.WebDriverEventListener;

public class WebEventListener implements WebDriverEventListener {

    @Override
    public void beforeNavigateTo(String url, WebDriver driver) {
        Log.info("Before navigating to: '" + url + "'");
    }

    @Override
    public void afterNavigateTo(String url, WebDriver driver) {
        Log.info("Navigated to:'" + url + "'");
    }

    @Override
    public void beforeClickOn(WebElement element, WebDriver driver) {
        Log.info("Trying to click on: " + element.toString());
    }

    @Override
    public void afterClickOn(WebElement element, WebDriver driver) {
        Log.info("Clicked on: " + element.toString());
    }

    @Override
    public void onException(Throwable error, WebDriver driver) {
        Log.info("Exception occured: " + error);
    }

    @Override
    public void beforeFindBy(By by, WebElement element, WebDriver driver) {
        Log.info("Trying to find Element By : " + by.toString());
    }

    @Override
    public void afterFindBy(By by, WebElement element, WebDriver driver) {
        Log.info("Found Element By : " + by.toString());
    }

    @Override
    public void beforeGetText(WebElement webElement, WebDriver webDriver) {
        Log.info("Trying to get text from: " + webElement.toString());
    }

    @Override
    public void afterGetText(WebElement webElement, WebDriver webDriver, String s) {
        Log.info("Got text from: " + webElement.toString());
    }

    @Override
    public void beforeSwitchToWindow(String s, WebDriver webDriver) {
        Log.info("Trying to switch window with id:  " + s);
    }

    @Override
    public void afterSwitchToWindow(String s, WebDriver webDriver) {
        Log.info("Switched to window with id:  " + s);
    }

    @Override
    public <X> void beforeGetScreenshotAs(OutputType<X> outputType) {
        Log.info("Trying to get screenshot:  " + outputType.toString());
    }

    @Override
    public <X> void afterGetScreenshotAs(OutputType<X> outputType, X x) {
        Log.info("Got screenshot:  " + outputType.toString());
    }

    @Override
    public void beforeNavigateForward(WebDriver driver) {

    }

    @Override
    public void afterNavigateForward(WebDriver driver) {

    }

    @Override
    public void beforeAlertAccept(WebDriver webDriver) {

    }

    @Override
    public void afterAlertAccept(WebDriver webDriver) {

    }

    @Override
    public void afterAlertDismiss(WebDriver webDriver) {

    }

    @Override
    public void beforeAlertDismiss(WebDriver webDriver) {

    }

    @Override
    public void beforeNavigateRefresh(WebDriver webDriver) {

    }

    @Override
    public void afterNavigateRefresh(WebDriver webDriver) {

    }

    @Override
    public void beforeChangeValueOf(WebElement webElement, WebDriver webDriver, CharSequence[] charSequences) {

    }

    @Override
    public void afterChangeValueOf(WebElement webElement, WebDriver webDriver, CharSequence[] charSequences) {

    }

    @Override
    public void beforeScript(String s, WebDriver webDriver) {

    }

    @Override
    public void afterScript(String s, WebDriver webDriver) {

    }

    @Override
    public void beforeNavigateBack(WebDriver driver) {

    }

    @Override
    public void afterNavigateBack(WebDriver driver) {

    }
}

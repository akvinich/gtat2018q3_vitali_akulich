package runner;

import listeners.TestListener;
import org.testng.TestNG;

import java.util.Arrays;
import java.util.List;

public class TestRunner {

    public static void main(String[] args) {
        TestNG testNG = new TestNG();
        testNG.addListener(new TestListener());
        List<String> files = Arrays.asList("./src/test/resources/testng.xml");
        testNG.setTestSuites(files);
        testNG.run();
    }
}

package listeners;

import org.testng.IInvokedMethod;
import org.testng.IInvokedMethodListener;
import org.testng.ITestResult;

public class TestListener implements IInvokedMethodListener {
    @Override
    public void beforeInvocation(IInvokedMethod iInvokedMethod, ITestResult iTestResult) {
        System.out.println("[METHOD_STARTED] - " + iInvokedMethod.getTestMethod().getMethodName());
    }

    @Override
    public void afterInvocation(IInvokedMethod iInvokedMethod, ITestResult iTestResult) {
        System.out.println(String.format("[METHOD_FINISHED] - %s >>> %s",
                iInvokedMethod.getTestMethod().getMethodName(), iTestResult.getStatus() == 1 ? "Success" : "Fail"));
    }
}

package tests;

import com.epam.gomel.homework.*;
import org.testng.annotations.*;
import org.testng.asserts.SoftAssert;

import static org.mockito.Mockito.*;

public class MockitoTest {
    Boy boy;
    SoftAssert softAssert = new SoftAssert();

    @Test(groups = {"mock"})
    @Parameters("excellent")
    public void expectExcellentMoodTest(String mood) {
        boy = mock(Boy.class);
        when(boy.isSummerMonth()).thenReturn(true).thenReturn(false);
        when(boy.isPrettyGirlFriend()).thenReturn(true).thenReturn(false);
        when(boy.isRich()).thenReturn(true).thenReturn(false);
        when(boy.getMood()).thenReturn(Mood.EXCELLENT).thenReturn(Mood.GOOD);
        softAssert.assertEquals(boy.isSummerMonth(), true);
        softAssert.assertEquals(boy.isPrettyGirlFriend(), true);
        softAssert.assertEquals(boy.isRich(), true);
        softAssert.assertEquals(boy.getMood().toString(), mood);
    }

    @Test(groups = {"mock"}, dependsOnMethods = {"expectExcellentMoodTest"})
    @Parameters("horrible")
    public void expectHorribleMoodTest(String mood) {
        softAssert.assertEquals(boy.isSummerMonth(), false);
        softAssert.assertEquals(boy.isPrettyGirlFriend(), false);
        softAssert.assertEquals(boy.isRich(), false);
        softAssert.assertEquals(boy.getMood().toString(), mood);
    }
}

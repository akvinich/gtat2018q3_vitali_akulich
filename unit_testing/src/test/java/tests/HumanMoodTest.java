package tests;

import com.epam.gomel.homework.Human;
import com.epam.gomel.homework.Mood;
import org.testng.Assert;
import org.testng.annotations.Test;

public class HumanMoodTest {

    private Human human;
    private Mood mood;

    public HumanMoodTest(Human human, Mood mood) {
        this.human = human;
        this.mood = mood;
    }

    @Test(alwaysRun = true)
    public void getHumanMoodTest() {
        Assert.assertEquals(human.getMood(), mood);
    }
}

package tests;

import com.epam.gomel.homework.*;

import org.testng.annotations.*;

import java.time.Month;

public class FactoryHumanTest {
    final double bigWealth = 5_000_000;
    final double borderWealth = 1_000_000;

    @DataProvider(name = "Data for human creation")
    public Object[][] createHumanData() {
        return new Object[][]{
                {new Boy(Month.AUGUST, 0), Mood.BAD},
                {new Boy(Month.JULY, 0, new Girl(false)), Mood.BAD},
                {new Boy(Month.JUNE, 0, new Girl(true)), Mood.BAD},
                {new Boy(Month.MAY, 0, new Girl(true)), Mood.BAD},
                {new Boy(Month.APRIL, bigWealth), Mood.BAD},
                {new Boy(Month.MARCH, bigWealth, new Girl(false)), Mood.BAD},
                {new Boy(Month.FEBRUARY, 0), Mood.HORRIBLE},
                {new Boy(Month.JANUARY, 0, new Girl(false)), Mood.HORRIBLE},
                {new Boy(Month.AUGUST, borderWealth), Mood.NEUTRAL},
                {new Boy(Month.JUNE, borderWealth, new Girl(false)), Mood.NEUTRAL},
                {new Boy(Month.MAY, borderWealth, new Girl(true)), Mood.GOOD},
                {new Boy(Month.JULY, bigWealth, new Girl(true)), Mood.EXCELLENT},
                {new Girl(true, true), Mood.GOOD},
                {new Girl(true, true, new Boy(Month.APRIL, bigWealth)), Mood.EXCELLENT},
                {new Girl(true, true, new Boy(Month.MARCH, 0)), Mood.GOOD},
                {new Girl(true, false), Mood.GOOD},
                {new Girl(true, false, new Boy(Month.AUGUST, bigWealth)), Mood.EXCELLENT},
                {new Girl(true, false, new Boy(Month.AUGUST, 0)), Mood.GOOD},
                {new Girl(false, false), Mood.I_HATE_THEM_ALL},
                {new Girl(false, false,  new Boy(Month.AUGUST, bigWealth)), Mood.GOOD},
                {new Girl(false, false,  new Boy(Month.AUGUST, 0)), Mood.I_HATE_THEM_ALL},
                {new Girl(false, true), Mood.NEUTRAL},
                {new Girl(false, true,  new Boy(Month.AUGUST, borderWealth)), Mood.GOOD},
                {new Girl(false, true,  new Boy(Month.AUGUST, 0)), Mood.NEUTRAL},
        };
    }

    @Factory(dataProvider = "Data for human creation")
    public Object[] createBoyTest(Human human, Mood mood) {
        return new Object[] {new HumanMoodTest(human, mood)};
    }

}

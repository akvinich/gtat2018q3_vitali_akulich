package tests;

import com.epam.gomel.homework.Boy;
import com.epam.gomel.homework.Girl;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.time.Month;

import static matchers.BoyMatchers.*;
import static matchers.GirlMatchers.hasBoyFriend;
import static matchers.GirlMatchers.isGirlGotAFewKilos;
import static matchers.GirlMatchers.isPrettyGirl;
import static org.hamcrest.CoreMatchers.both;
import static org.hamcrest.MatcherAssert.assertThat;

public class InitTest {
    final double bigWealth = 5_000_000;
    final double normalWealth = 1_000_000;
    final double smallWealth = 100;
    final int priorityOfGirlTest = 5;
    final int priorityOfBoyTest = 0;

    @DataProvider
    public Object[][] boyConstructorWithOneParameterData() {
        return new Object[][]{
                {Month.APRIL, 0, null},
                {Month.JANUARY, 0, null},
                {Month.AUGUST, 0, null},
                {Month.OCTOBER, 0, null},
        };
    }

    @DataProvider
    public Object[][] boyConstructorWithTwoParametersData() {
        return new Object[][]{
                {Month.MAY, smallWealth, null},
                {Month.DECEMBER, bigWealth, null},
                {Month.DECEMBER, normalWealth, null},
                {Month.JUNE, 0, null},
                {Month.SEPTEMBER, -1, null},
        };
    }

    @DataProvider
    public Object[][] boyConstructorWithThreeParametersData() {
        return new Object[][]{
                {Month.MARCH, smallWealth, new Girl()},
                {Month.FEBRUARY, bigWealth, new Girl(true)},
                {Month.FEBRUARY, normalWealth, new Girl(true)},
                {Month.JULY, 0, new Girl(false, true)},
                {Month.NOVEMBER, -smallWealth, new Girl(true, false, new Boy(Month.MAY))},
        };
    }

    @DataProvider
    public Object[][] girlEmptyConstructorData() {
        return new Object[][]{
                {false, true, null},
        };
    }

    @DataProvider
    public Object[][] girlConstructorWithOneParameterData() {
        return new Object[][]{
                {true, false, null},
                {false, false, null},
        };
    }

    @DataProvider
    public Object[][] girlConstructorWithTwoParametersData() {
        return new Object[][]{
                {true, true, null},
                {false, false, null},
                {false, true, null},
                {true, false, null},
        };
    }

    @DataProvider
    public Object[][] girlConstructorWithThreeParametersData() {
        return new Object[][]{
                {true, true, new Boy(Month.MAY)},
                {false, false, new Boy(Month.AUGUST, bigWealth)},
                {false, true, new Boy(Month.JANUARY, 0, new Girl(true, false))},
                {true, false, null},
        };
    }

    @Test(dataProvider = "boyConstructorWithOneParameterData", groups = {"init", "boy"},
            priority = priorityOfBoyTest)
    public void boyConstructorWithOneParameterTest(Month birthdayMonth, double wealth, Girl girlFriend) {
        Boy boy = new Boy(birthdayMonth);
        assertThat(boy, both(hasMonth(birthdayMonth)).and(hasWealth(wealth)).and(hasGirlFriend(girlFriend)));
    }

    @Test(dataProvider = "boyConstructorWithTwoParametersData", groups = {"init", "boy"},
            priority = priorityOfBoyTest)
    public void boyConstructorWithTwoParametersTest(Month birthdayMonth, double wealth, Girl girlFriend) {
        Boy boy = new Boy(birthdayMonth, wealth);
        assertThat(boy, both(hasMonth(birthdayMonth)).and(hasWealth(wealth)).and(hasGirlFriend(girlFriend)));
    }

    @Test(dataProvider = "boyConstructorWithThreeParametersData", groups = {"init", "boy"},
            priority = priorityOfBoyTest)
    public void boyConstructorWithThreeParametersTest(Month birthdayMonth, double wealth, Girl girlFriend) {
        Boy boy = new Boy(birthdayMonth, wealth, girlFriend);
        assertThat(boy, both(hasMonth(birthdayMonth)).and(hasWealth(wealth)).and(hasGirlFriend(girlFriend)));
    }

    @Test(dataProvider = "girlEmptyConstructorData", groups = {"init", "girl"},
            priority = priorityOfGirlTest)
    public void girlEmptyConstructorTest(boolean isPretty,  boolean isSlimFriendGotAFewKilos, Boy boyFriend) {
        Girl girl = new Girl();
        assertThat(girl, both(isPrettyGirl(isPretty)).and(isGirlGotAFewKilos(isSlimFriendGotAFewKilos))
                .and(hasBoyFriend(boyFriend)));
    }

    @Test(dataProvider = "girlConstructorWithOneParameterData", groups = {"init", "girl"},
            priority = priorityOfGirlTest)
    public void girlConstructorWithOneParametersTest(boolean isPretty,  boolean isSlimFriendGotAFewKilos,
                                                     Boy boyFriend) {
        Girl girl = new Girl(isPretty);
        assertThat(girl, both(isPrettyGirl(isPretty)).and(isGirlGotAFewKilos(isSlimFriendGotAFewKilos))
                .and(hasBoyFriend(boyFriend)));
    }

    @Test(dataProvider = "girlConstructorWithTwoParametersData", groups = {"init", "girl"},
            priority = priorityOfGirlTest)
    public void girlConstructorWithTwoParametersTest(boolean isPretty,  boolean isSlimFriendGotAFewKilos,
                                                     Boy boyFriend) {
        Girl girl = new Girl(isPretty, isSlimFriendGotAFewKilos);
        assertThat(girl, both(isPrettyGirl(isPretty)).and(isGirlGotAFewKilos(isSlimFriendGotAFewKilos))
                .and(hasBoyFriend(boyFriend)));
    }

    @Test(dataProvider = "girlConstructorWithThreeParametersData", groups = {"init", "girl"},
            priority = priorityOfGirlTest)
    public void girlConstructorWithThreeParametersTest(boolean isPretty,  boolean isSlimFriendGotAFewKilos,
                                                       Boy boyFriend) {
        Girl girl = new Girl(isPretty, isSlimFriendGotAFewKilos, boyFriend);
        assertThat(girl, both(isPrettyGirl(isPretty)).and(isGirlGotAFewKilos(isSlimFriendGotAFewKilos))
                .and(hasBoyFriend(boyFriend)));
    }
}

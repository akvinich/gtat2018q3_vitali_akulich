package tests;

import base.BaseTest;
import com.epam.gomel.homework.*;
import org.testng.Assert;
import org.testng.annotations.*;

import java.time.Month;

public class GirlSimpleMethodsTest extends BaseTest {

    @DataProvider
    public Object[][] isSlimFriendBecameFatData() {
        return new Object[][]{
                {new Girl(true, true), false},
                {new Girl(false, true), true},
                {new Girl(false, false), false},
                {new Girl(true, false), false},
        };
    }

    @DataProvider
    public Object[][] isBoyFriendWillBuyNewShoesData() {
        return new Object[][]{
                {new Girl(true, false, new Boy(Month.JULY, bigWealth)), true},
                {new Girl(true, false), false},
                {new Girl(true, false, new Boy(Month.JULY, smallWealth)), false},
                {new Girl(false, false, new Boy(Month.JULY, bigWealth)), false},
                {new Girl(false, false), false},
                {new Girl(false, false, new Boy(Month.JULY, smallWealth)), false},
        };
    }

    @DataProvider
    public Object[][] isBoyfriendRichData() {
        return new Object[][]{
                {new Girl(true, false, new Boy(Month.JULY, bigWealth)), true},
                {new Girl(true, false, new Boy(Month.JULY, normalWealth)), true},
                {new Girl(true, false, new Boy(Month.JULY, smallWealth)), false},
                {new Girl(true, false), false},
        };
    }

    @DataProvider
    public Object[][] spendBoyFriendMoneyData() {
        return new Object[][]{
                {new Girl(true, false, new Boy(Month.JULY, bigWealth)), bigWealth, 0},
                {new Girl(true, false, new Boy(Month.JULY, normalWealth)), normalWealth, 0},
        };
    }

    @DataProvider
    public Object[][] spendBoyFriendMoneyExpectedExceptionsData() {
        return new Object[][]{
                {new Girl(true, false, new Boy(Month.JULY, normalWealth)), bigWealth},
        };
    }

    @Test(dataProvider = "isSlimFriendBecameFatData", groups = {"girl"})
    public void isSlimFriendBecameFatTest(Girl girl, boolean expectResult) {
        Assert.assertEquals(girl.isSlimFriendBecameFat(), expectResult);
    }

    @Test(dataProvider = "isBoyFriendWillBuyNewShoesData", groups = {"girl"})
    public void isBoyFriendWillBuyNewShoesDataTest(Girl girl, boolean expectResult) {
        Assert.assertEquals(girl.isBoyFriendWillBuyNewShoes(), expectResult);
    }

    @Test(dataProvider = "isBoyfriendRichData", groups = {"girl"})
    public void isBoyfriendRichTest(Girl girl, boolean expectResult) {
        Assert.assertEquals(girl.isBoyfriendRich(), expectResult);
    }

    @Test(dataProvider = "spendBoyFriendMoneyData", groups = {"girl"})
    public void spendBoyFriendMoneyTest(Girl girl, double amountForSpending, double expectWealth) {
        girl.spendBoyFriendMoney(amountForSpending);
        Assert.assertEquals(girl.getBoyFriend().getWealth(), expectWealth);
    }

    @Test(dataProvider = "spendBoyFriendMoneyExpectedExceptionsData", expectedExceptions = RuntimeException.class,
            groups = {"girl"})
    public void spendBoyFriendMoneyExpectedExceptionTest(Girl girl, double amountForSpending) {
        girl.spendBoyFriendMoney(amountForSpending);
    }

    @Test(groups = {"girl"})
    public void setPrettyGirlTest() {
        Girl girl = new Girl(true, true, new Boy(Month.JULY));
        Assert.assertEquals(girl.isPretty(), true);
        girl.setPretty(false);
        Assert.assertEquals(girl.isPretty(), false);
    }
}

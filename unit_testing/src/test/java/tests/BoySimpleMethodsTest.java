package tests;

import base.BaseTest;
import com.epam.gomel.homework.*;
import org.testng.Assert;
import org.testng.annotations.*;

import java.time.Month;

public class BoySimpleMethodsTest extends BaseTest {

    @DataProvider
    public Object[][] isRichBoyData() {
        return new Object[][]{
                {new Boy(Month.FEBRUARY, bigWealth), true},
                {new Boy(Month.FEBRUARY, normalWealth), true},
                {new Boy(Month.FEBRUARY, smallWealth), false},
                {new Boy(Month.FEBRUARY, 0), false},
                {new Boy(Month.FEBRUARY, -1), false},
        };
    }

    @DataProvider
    public Object[][] isPrettyGirlFriendBoyData() {
        return new Object[][]{
                {new Boy(Month.FEBRUARY, bigWealth, new Girl(true)), true},
                {new Boy(Month.FEBRUARY, bigWealth, new Girl(false)), false},
                {new Boy(Month.FEBRUARY, bigWealth), false},
        };
    }

    @DataProvider
    public Object[][] isSummerMonthBoyData() {
        return new Object[][]{
                {new Boy(Month.FEBRUARY), false},
                {new Boy(Month.JANUARY), false},
                {new Boy(Month.MARCH), false},
                {new Boy(Month.APRIL), false},
                {new Boy(Month.MAY), false},
                {new Boy(Month.JUNE), true},
                {new Boy(Month.JULY), true},
                {new Boy(Month.AUGUST), true},
                {new Boy(Month.SEPTEMBER), false},
                {new Boy(Month.OCTOBER), false},
                {new Boy(Month.NOVEMBER), false},
                {new Boy(Month.DECEMBER), false},
        };
    }

    @DataProvider
    public Object[][] spendSomeMoneyBoyData() {
        return new Object[][]{
                {new Boy(Month.FEBRUARY, bigWealth), normalWealth, bigWealth - normalWealth},
                {new Boy(Month.FEBRUARY, normalWealth), smallWealth, normalWealth - smallWealth},
                {new Boy(Month.FEBRUARY, smallWealth), smallWealth, 0},
        };
    }

    @DataProvider
    public Object[][] spendSomeMoneyExpectedExceptionData() {
        return new Object[][]{
                {new Boy(Month.FEBRUARY, 0), smallWealth},
        };
    }

    @Test(dataProvider = "isRichBoyData", groups = {"boy"})
    public void isRichBoyTest(Boy boy, boolean expectResult) {
        Assert.assertEquals(boy.isRich(), expectResult);
    }

    @Test(dataProvider = "isPrettyGirlFriendBoyData", groups = {"boy"})
    public void isPrettyGirlFriendTest(Boy boy, boolean expectResult) {
        Assert.assertEquals(boy.isPrettyGirlFriend(), expectResult);
    }

    @Test(dataProvider = "isSummerMonthBoyData", groups = {"boy"})
    public void isSummerMonthTest(Boy boy, boolean expectResult) {
        Assert.assertEquals(boy.isSummerMonth(), expectResult);
    }

    @Test(dataProvider = "spendSomeMoneyBoyData", groups = {"boy"})
    public void spendSomeMoneyTest(Boy boy, double wealth, double expectWealth) {
        boy.spendSomeMoney(wealth);
        Assert.assertEquals(boy.getWealth(), expectWealth);
    }

    @Test(dataProvider = "spendSomeMoneyExpectedExceptionData", expectedExceptions = RuntimeException.class,
            groups = {"boy"})
    public void spendSomeMoneyExpectedExceptionTest(Boy boy, double wealth) {

        boy.spendSomeMoney(wealth);
    }

}

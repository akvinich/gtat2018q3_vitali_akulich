package base;

public class BaseTest {
    protected final double bigWealth = 5_000_000;
    protected final double normalWealth = 1_000_000;
    protected final double smallWealth = 100;
}

package matchers;

import com.epam.gomel.homework.Boy;
import com.epam.gomel.homework.Girl;
import org.hamcrest.FeatureMatcher;
import org.hamcrest.Matcher;

import java.time.Month;

import static org.hamcrest.core.IsSame.sameInstance;
import static org.hamcrest.core.IsEqual.equalTo;

public class BoyMatchers {

    public static Matcher<Boy> hasMonth(final Month birthdayMonth) {
        return new FeatureMatcher<Boy, Month>(equalTo(birthdayMonth), "boy has birthdayMonth - ", "birthdayMonth -") {
            @Override
            protected Month featureValueOf(Boy boy) {
                return boy.getBirthdayMonth();
            }
        };
    }

    public static Matcher<Boy> hasWealth(final double wealth) {
        return new FeatureMatcher<Boy, Double>(equalTo(wealth), "boy has wealth - ", "wealth -") {
            @Override
            protected Double featureValueOf(Boy boy) {
                return boy.getWealth();
            }
        };
    }

    public static Matcher<Boy> hasGirlFriend(final Girl girlFriend) {
        return new FeatureMatcher<Boy, Girl>(sameInstance(girlFriend), "boy has girlfriend - ", "girlfriend -") {
            @Override
            protected Girl featureValueOf(Boy boy) {
                return boy.getGirlFriend();
            }
        };
    }

}

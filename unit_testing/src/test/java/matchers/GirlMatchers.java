package matchers;

import com.epam.gomel.homework.Boy;
import com.epam.gomel.homework.Girl;
import org.hamcrest.FeatureMatcher;
import org.hamcrest.Matcher;

import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsSame.sameInstance;

public class GirlMatchers {
    public static Matcher<Girl> isPrettyGirl(final Boolean isPretty) {
        return new FeatureMatcher<Girl, Boolean>(is(isPretty),
                "girl is pretty - ", "pretty -") {
            @Override
            protected Boolean featureValueOf(Girl girl) {
                return girl.isPretty();
            }
        };
    }

    public static Matcher<Girl> isGirlGotAFewKilos(final Boolean isSlimFriendGotAFewKilos) {
        return new FeatureMatcher<Girl, Boolean>(is(isSlimFriendGotAFewKilos),
                "girl get a few kilos - ", "a few kilos -") {
            @Override
            protected Boolean featureValueOf(Girl girl) {
                return girl.isSlimFriendGotAFewKilos();
            }
        };
    }

    public static Matcher<Girl> hasBoyFriend(final Boy boyFriend) {
        return new FeatureMatcher<Girl, Boy>(sameInstance(boyFriend),
                "girl has boyfriend - ", "boyfriend -") {
            @Override
            protected Boy featureValueOf(Girl girl) {
                return girl.getBoyFriend();
            }
        };
    }
}
